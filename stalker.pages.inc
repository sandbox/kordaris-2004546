<?php
/**
 * @file
 * Menu callbacks for stalker module.
 */

/**
 * Page callback function for user/%user/stalkers menu path.
 */
function stalker_user_page($account) {
  $result = db_select('stalker', 's')
    ->fields('s', array('stalker_uid', 'view_count'))
    ->condition('s.profile_uid', $account->uid, '=')
    ->orderBy('view_count', 'DESC')
    ->execute();

  $i = 0;
  $user_account = array();
  $items = array();
  foreach ($result as $row) {
    if (($i == 5) || ($row->view_count < variable_get('stalker_threshold', 1))) {
      break;
    }
    if (!variable_get('stalker_identity', FALSE)) {
      $user_account[] = user_load($row->stalker_uid);
      $items[] = theme('username', array('account' => $user_account[$i])) . " : " . $row->view_count;
      $i++;
    }
    else {
      $items[] = variable_get('anonymous', t('Anonymous')) . " : " . $row->view_count;
    }
  }

  return theme('item_list', array('items' => $items));
}
