Lists the number of times a user's profile has been viewed by other users. 
Stalker module has the ability to show the Top Stalkers of each user profile and
their number of  profile hits. Handy for spotting stalkers and secret admirers.

This module was created as an example module for use in Lullabot's Drupal Module
Development tutorial DVD and video download.
