<?php
/**
 * @file
 * Install file for stalker module.
 */

/**
 * Implements hook_schema().
 *
 * Describe the module's data model as an associative array. This removes
 * the requirement to write database-specific SQL to create tables.
 */
function stalker_schema() {
  $schema = array();

  $schema['stalker'] = array(
    'description' => 'Contain counter information for profile views.',
    'fields' => array(
      'stalker_uid' => array(
        'description' => 'The {users}.uid of the user viewing the profile.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'profile_uid' => array(
        'description' => 'The {users}.uid of the profile being viewed.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'view_count' => array(
        'description' => 'Simple counter per user.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'last_stalked' => array(
        'description' => 'The last time this user stalked.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('stalker_uid', 'profile_uid'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 *
 * Perform final clean-up tasks.
 */
function stalker_uninstall() {
  drupal_uninstall_schema('stalker');
  variable_del('stalker_threshold');
  variable_del('stalker_identity');
  variable_del('stalker_narcissism');
}

/**
 * Implementation of an update hook.
 */
function stalker_update_7001() {
  $spec = array(
    'description' => t('The last time this user stalked.'),
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'default' => 0,
  );
  db_add_field('stalker', 'last_stalked', $spec);
}
